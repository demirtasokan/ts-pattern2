import { CharactersCollection } from './CharactersCollection';
import { NumbersCollection } from './NumbersCollection';
import { Sorter } from './Sorter';

// const numbersCollection = new NumbersCollection([50, 3, -5, 0]);
// const sorter = new Sorter(numbersCollection);

// sorter.sort();

// console.log(sorter.collection);

const charactersCollection = new CharactersCollection('Xaaybn');
const sorter = new Sorter(charactersCollection);
sorter.sort();
console.log(charactersCollection.data);
